import type { Metadata } from "next";
import { Inter as FontSans } from "next/font/google";
import "@/app/globals.css";

import Link from "next/link";
import { cn } from "@/app/lib/utils";
import { ThemeProvider } from "@/app/components/theme-provider";

import { ThemeToggle } from "@/app/components/theme-toggle";
import Providers from "@/app/providers";

const fontSans = FontSans({
  subsets: ["latin"],
  variable: "--font-sans",
});

export const metadata: Metadata = {
  title: "Hotel",
  description: "An admin panel for a hotel",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body
        className={cn(
          "min-h-screen bg-background font-sans antialiased",
          fontSans.variable,
        )}
      >
        <ThemeProvider
          attribute="class"
          defaultTheme="system"
          enableSystem
          disableTransitionOnChange
        >
          <header className="fixed top-0 left-0 w-screen p-4 h-20 backdrop-blur-lg flex flex-row items-center gap-4 border-b z-40">
            <Link href="/" className="text-2xl bold">
              Hotel
            </Link>
            <Link href="/customers" className="text-l h-fit">
              Customers
            </Link>
            <Link href="/rooms" className="text-l h-fit">
              Rooms
            </Link>
            <Link href="/bookings" className="text-l h-fit">
              Bookings
            </Link>
            <div className="ml-auto">
              <ThemeToggle />
            </div>
          </header>
          <Providers>
            <main className="pt-20">{children}</main>
          </Providers>
        </ThemeProvider>
      </body>
    </html>
  );
}
