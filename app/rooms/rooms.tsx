"use client";

import { useState, Dispatch, SetStateAction, useEffect } from "react";
import { Input } from "../components/ui/input";
import { TableCell, TableRow } from "@/app/components/ui/table";
import { Button } from "../components/ui/button";
import { atom, useAtom, useSetAtom } from "jotai";

export type Room = {
  id: number;
  number: string;
  capacity: number;
  comfort_score: number;
  price: number;
};
export const DefaultRoom = {
  id: 0,
  number: "",
  capacity: 1,
  comfort_score: 1,
  price: 1,
};

export const rooms = atom<Room[]>([]);

export function RoomInputRow({
  roomState,
  actions,
}: {
  roomState: [Room, Dispatch<SetStateAction<Room>>];
  actions: { title: string; onClick: (room: Room) => Room }[];
}) {
  const [room, setRoom] = roomState;

  return (
    <TableRow>
      <TableCell className="align-top">
        {room.id === 0 ? "" : room.id}
      </TableCell>
      <TableCell className="align-top">
        <Input
          placeholder="Lorem"
          value={room.number}
          onChange={(e) =>
            setRoom((old) => {
              return { ...old, number: e.target.value };
            })
          }
        />
      </TableCell>
      <TableCell className="align-top">
        <Input
          type="number"
          value={room.capacity}
          min={1}
          onChange={(e) =>
            setRoom((old) => {
              return { ...old, capacity: +e.target.value };
            })
          }
        />
      </TableCell>
      <TableCell className="align-top">
        <Input
          type="number"
          value={room.comfort_score}
          min={1}
          onChange={(e) =>
            setRoom((old) => {
              return { ...old, comfort_score: +e.target.value };
            })
          }
        />
      </TableCell>
      <TableCell className="align-top">
        <Input
          type="number"
          value={room.price}
          min={1}
          onChange={(e) =>
            setRoom((old) => {
              return { ...old, price: +e.target.value };
            })
          }
        />
      </TableCell>
      <TableCell className="flex gap-2">
        {actions.map((action, i) => {
          return (
            <Button
              key={i}
              variant={i === 0 ? "default" : "secondary"}
              onClick={() => {
                setRoom(action.onClick(room));
              }}
              className="grow"
            >
              {action.title}
            </Button>
          );
        })}
      </TableCell>
    </TableRow>
  );
}

export function AddRoomRow() {
  const setRooms = useSetAtom(rooms);
  const roomState = useState<Room>(DefaultRoom);

  const onAdd = (room: Room) => {
    var inserted = 0;
    setRooms((old) => {
      inserted = old.length;
      return old.concat([room]);
    });

    const request = new Request("http://127.0.0.1:1323/rooms", {
      method: "POST",
      body: JSON.stringify(room),
      headers: {
        "Content-Type": "application/json",
      },
    });

    fetch(request).then(async (response) => {
      const json = await response.json();
      setRooms((old) => old.map((o, i) => (i === inserted ? json : o)));
    });

    return DefaultRoom;
  };
  const onClear = (_: Room) => {
    return DefaultRoom;
  };
  return (
    <RoomInputRow
      roomState={roomState}
      actions={[
        { title: "Add", onClick: onAdd },
        { title: "Clear", onClick: onClear },
      ]}
    />
  );
}

export function RoomRow({ room }: { room: Room }) {
  const setRooms = useSetAtom(rooms);
  const [edit, setEdit] = useState<Room>(DefaultRoom);

  const onEdit = () => {
    setEdit(room);
  };

  const onDelete = () => {
    setRooms((old) => old.filter((o) => o.id !== room.id));

    fetch("http://127.0.0.1:1323/rooms/" + room.id, {
      method: "DELETE",
    });
  };

  const onSave = (room: Room) => {
    setRooms((old) =>
      old.map((o) => (o.id === room.id ? room : o)),
    );
    setEdit(DefaultRoom);

    fetch("http://127.0.0.1:1323/rooms/" + room.id, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(room),
    });

    return DefaultRoom;
  };

  const onCancel = (_: Room) => {
    setEdit(DefaultRoom);

    return DefaultRoom;
  };

  if (edit.id !== 0) {
    return (
      <RoomInputRow
        roomState={[edit, setEdit]}
        actions={[
          { title: "Save", onClick: onSave },
          { title: "Cancel", onClick: onCancel },
        ]}
      />
    );
  }

  return (
    <TableRow>
      <TableCell className="font-medium">{room.id}</TableCell>
      <TableCell>{room.number}</TableCell>
      <TableCell>{room.capacity}</TableCell>
      <TableCell>{room.comfort_score}</TableCell>
      <TableCell>${room.price}</TableCell>
      <TableCell className="flex gap-2">
        <Button className="grow rounded" onClick={onEdit}>
          Edit
        </Button>
        <Button
          className="grow rounded"
          variant="destructive"
          onClick={onDelete}
        >
          Delete
        </Button>
      </TableCell>
    </TableRow>
  );
}

export function RoomList({ initial }: { initial: Room[] }) {
  const [items, setItems] = useAtom(rooms);

  useEffect(() => {
    setItems(initial);
  }, []);

  return items.map((room) => <RoomRow room={room} key={room.id} />);
}
