"use server";

import {
  Table,
  TableBody,
  TableCaption,
  TableHead,
  TableHeader,
  TableRow,
} from "@/app/components/ui/table";
import { AddRoomRow, Room, RoomList } from "./rooms";

export default async function Rooms() {
  const rooms: Room[] = await fetch("http://127.0.0.1:1323/rooms", {
    // next: { tags: ["rooms"] },
    cache: "no-store",
  }).then((response) => response.json());

  return (
    <div className="m-24 rounded p-2 border shadow">
      <Table>
        <TableCaption>A list of your rooms.</TableCaption>
        <TableHeader>
          <TableRow>
            <TableHead className="w-10">Id</TableHead>
            <TableHead>Number</TableHead>
            <TableHead>Capacity</TableHead>
            <TableHead>Comfort score</TableHead>
            <TableHead>Price</TableHead>
            <TableHead>Actions</TableHead>
          </TableRow>
        </TableHeader>
        <TableBody>
          <RoomList initial={rooms} />
          <AddRoomRow />
        </TableBody>
      </Table>
    </div>
  );
}
