"use server";

import {
  Table,
  TableBody,
  TableCaption,
  TableHead,
  TableHeader,
  TableRow,
} from "@/app/components/ui/table";
import { Room } from "../rooms/rooms";
import { Customer } from "../customers/customers";
import { AddBookingRow, Booking, BookingList } from "./bookings";

export default async function Bookings() {
  const rooms: Room[] = await fetch("http://127.0.0.1:1323/rooms", {
    cache: "no-store",
  }).then((response) => response.json());
  const customers: Customer[] = await fetch("http://127.0.0.1:1323/customers", {
    cache: "no-store",
  }).then((response) => response.json());
  const bookings: Booking[] = await fetch("http://127.0.0.1:1323/bookings", {
    cache: "no-store",
  }).then((response) => response.json());

  return (
    <div className="m-24 rounded p-2 border shadow">
      <Table>
        <TableCaption>A list of bookings.</TableCaption>
        <TableHeader>
          <TableRow>
            <TableHead className="w-10">Id</TableHead>
            <TableHead>Customer</TableHead>
            <TableHead>Room</TableHead>
            <TableHead>Booked on</TableHead>
            <TableHead>Arrived on</TableHead>
            <TableHead>Departed on</TableHead>
            <TableHead>Actions</TableHead>
          </TableRow>
        </TableHeader>
        <TableBody>
          <BookingList initialBookings={bookings} initialRooms={rooms} initialCustomers={customers} />
          <AddBookingRow />
        </TableBody>
      </Table>
    </div>
  );
}
