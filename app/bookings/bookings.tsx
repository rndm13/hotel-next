"use client";

import { useState, Dispatch, SetStateAction, useEffect } from "react";
import { TableCell, TableRow } from "@/app/components/ui/table";
import { Button } from "../components/ui/button";
import { atom, useAtom, useAtomValue, useSetAtom } from "jotai";
import { Customer, customers, DefaultCustomer } from "../customers/customers";
import { Room, rooms, DefaultRoom } from "../rooms/rooms";
import { CalendarIcon } from "lucide-react";
import { Calendar } from "@/app/components/ui/calendar";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/app/components/ui/select";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/app/components/ui/popover";
import moment from "moment";

export type Booking = {
  id: number;
  customer: Customer;
  room: Room;
  booked_on: Date;
  arrived_on?: Date;
  departed_on?: Date;
};

export const DefaultBooking = {
  id: 0,
  customer: DefaultCustomer,
  room: DefaultRoom,
  booked_on: new Date(),
};

export const bookings = atom<Booking[]>([]);

export function BookingInputRow({
  bookingState,
  actions,
}: {
  bookingState: [Booking, Dispatch<SetStateAction<Booking>>];
  actions: { title: string; onClick: (booking: Booking) => Booking }[];
}) {
  const [booking, setBooking] = bookingState;
  const customerList = useAtomValue(customers);
  const roomList = useAtomValue(rooms);

  return (
    <TableRow>
      <TableCell className="align-top">
        {booking.id === 0 ? "" : booking.id}
      </TableCell>
      <TableCell className="align-top">
        <Select
          value={booking.customer.id === 0 ? undefined : booking.customer.id.toString()}
          onValueChange={(new_val) => {
            setBooking({
              ...booking,
              customer:
                customerList.find((customer) => customer.id === +new_val) ??
                booking.customer,
            });
          }}
        >
          <SelectTrigger className="h-full">
            <SelectValue placeholder="Customer" />
          </SelectTrigger>
          <SelectContent>
            {customerList.map((customer) => {
              return (
                <SelectItem value={customer.id.toString()}>
                  {customer.first_name + " " + customer.last_name}
                </SelectItem>
              );
            })}
          </SelectContent>
        </Select>
      </TableCell>
      <TableCell className="align-top">
        <Select
          value={booking.room.id === 0 ? undefined : booking.room.id.toString()}
          onValueChange={(new_val) => {
            setBooking({
              ...booking,
              room:
                roomList.find((room) => room.id === +new_val) ?? booking.room,
            });
          }}
        >
          <SelectTrigger className="h-full">
            <SelectValue placeholder="Room"/>
          </SelectTrigger>
          <SelectContent>
            {roomList.map((room) => {
              return (
                <SelectItem value={room.id.toString()}>
                  {room.number}
                </SelectItem>
              );
            })}
          </SelectContent>
        </Select>
      </TableCell>
      <TableCell className="align-top">
        <Popover>
          <PopoverTrigger asChild>
            <Button
              variant={"outline"}
              className="h-full w-full pl-3 text-left font-normal"
            >
              {moment(booking.booked_on).format("DD/MM/YYYY")}
              <CalendarIcon className="ml-auto h-4 w-4 opacity-50" />
            </Button>
          </PopoverTrigger>
          <PopoverContent className="w-auto p-0" align="start">
            <Calendar
              className="h-full w-full"
              mode="single"
              selected={booking.booked_on}
              onSelect={(date) => {
                if (date != undefined) {
                  setBooking({ ...booking, booked_on: date });
                }
              }}
              initialFocus
              disabled={(date) =>
                booking.arrived_on != null && date > booking.arrived_on
              }
            />
          </PopoverContent>
        </Popover>
      </TableCell>
      <TableCell className="align-top">
        <Popover>
          <PopoverTrigger asChild>
            <Button
              variant={"outline"}
              className="h-full w-full pl-3 text-left font-normal"
            >
              {booking.arrived_on == undefined
                ? "Pick a date"
                : moment(booking.arrived_on).format("DD/MM/YYYY")}
              <CalendarIcon className="ml-auto h-4 w-4 opacity-50" />
            </Button>
          </PopoverTrigger>
          <PopoverContent className="w-auto p-0" align="start">
            <Calendar
              className="h-full w-full"
              mode="single"
              selected={booking.arrived_on}
              onSelect={(date) => {
                setBooking({ ...booking, arrived_on: date });
              }}
              disabled={(date) => date > new Date() || date < booking.booked_on}
              initialFocus
            />
          </PopoverContent>
        </Popover>
      </TableCell>
      <TableCell>
        <Popover>
          <PopoverTrigger asChild>
            <Button
              variant={"outline"}
              className="h-full w-full pl-3 text-left font-normal"
            >
              {booking.departed_on == undefined
                ? "Pick a date"
                : moment(booking.departed_on).format("DD/MM/YYYY")}
              <CalendarIcon className="ml-auto h-4 w-4 opacity-50" />
            </Button>
          </PopoverTrigger>
          <PopoverContent className="w-auto p-0" align="start">
            <Calendar
              className="h-full w-full"
              mode="single"
              selected={booking.departed_on}
              onSelect={(date) => {
                setBooking({ ...booking, departed_on: date });
              }}
              disabled={(date) =>
                booking.arrived_on == null ||
                date > new Date() ||
                date < booking.arrived_on
              }
              initialFocus
            />
          </PopoverContent>
        </Popover>
      </TableCell>
      <TableCell className="flex gap-2">
        {actions.map((action, i) => {
          return (
            <Button
              key={i}
              variant={i === 0 ? "default" : "secondary"}
              onClick={() => {
                setBooking(action.onClick(booking));
              }}
              className="grow"
            >
              {action.title}
            </Button>
          );
        })}
      </TableCell>
    </TableRow>
  );
}

export function AddBookingRow() {
  const setBookings = useSetAtom(bookings);
  const bookingState = useState<Booking>(DefaultBooking);

  const onAdd = (booking: Booking) => {
    var inserted = 0;
    setBookings((old) => {
      inserted = old.length;
      return old.concat([booking]);
    });

    const request = new Request("http://127.0.0.1:1323/bookings", {
      method: "POST",
      body: JSON.stringify({
        booking,
        customer_id: booking.customer.id,
        room_id: booking.room.id,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    });

    fetch(request).then(async (response) => {
      const json = await response.json();
      setBookings((old) =>
        old.map((o, i) => (i === inserted ? { ...o, id: json.id } : o)),
      );
    });

    return DefaultBooking;
  };
  const onClear = (_: Booking) => {
    return DefaultBooking;
  };
  return (
    <BookingInputRow
      bookingState={bookingState}
      actions={[
        { title: "Add", onClick: onAdd },
        { title: "Clear", onClick: onClear },
      ]}
    />
  );
}

export function BookingRow({ booking }: { booking: Booking }) {
  const setBookings = useSetAtom(bookings);
  const [edit, setEdit] = useState<Booking>(DefaultBooking);

  const onEdit = () => {
    setEdit(booking);
  };

  const onDelete = () => {
    setBookings((old) => old.filter((o) => o.id !== booking.id));

    fetch("http://127.0.0.1:1323/bookings/" + booking.id, {
      method: "DELETE",
    });
  };

  const onSave = (booking: Booking) => {
    setBookings((old) => old.map((o) => (o.id === booking.id ? booking : o)));
    setEdit(DefaultBooking);

    fetch("http://127.0.0.1:1323/bookings/" + booking.id, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(booking),
    });

    return DefaultBooking;
  };

  const onCancel = (_: Booking) => {
    setEdit(DefaultBooking);

    return DefaultBooking;
  };

  if (edit.id !== 0) {
    return (
      <BookingInputRow
        bookingState={[edit, setEdit]}
        actions={[
          { title: "Save", onClick: onSave },
          { title: "Cancel", onClick: onCancel },
        ]}
      />
    );
  }

  return (
    <TableRow>
      <TableCell className="font-medium">{booking.id}</TableCell>
      <TableCell>
        {booking.customer.first_name + " " + booking.customer.last_name}
      </TableCell>
      <TableCell>{booking.room.number}</TableCell>
      <TableCell>{moment(booking.booked_on).format("DD/MM/YYYY")}</TableCell>
      <TableCell>
        {booking.arrived_on
          ? moment(booking.arrived_on).format("DD/MM/YYYY")
          : ""}
      </TableCell>
      <TableCell>
        {booking.departed_on
          ? moment(booking.departed_on).format("DD/MM/YYYY")
          : ""}
      </TableCell>
      <TableCell className="flex gap-2">
        <Button className="grow rounded" onClick={onEdit}>
          Edit
        </Button>
        <Button
          className="grow rounded"
          variant="destructive"
          onClick={onDelete}
        >
          Delete
        </Button>
      </TableCell>
    </TableRow>
  );
}

export function BookingList({
  initialBookings,
  initialRooms,
  initialCustomers,
}: {
  initialBookings: Booking[];
  initialRooms: Room[];
  initialCustomers: Customer[];
}) {
  const [items, setItems] = useAtom(bookings);
  const setCustomers = useSetAtom(customers);
  const setRooms = useSetAtom(rooms);

  useEffect(() => {
    setItems(initialBookings);
    setCustomers(initialCustomers);
    setRooms(initialRooms);
  }, []);

  return items.map((booking) => (
    <BookingRow booking={booking} key={booking.id} />
  ));
}
