import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/app/components/ui/card";
import { Button } from "./components/ui/button";

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <div className="w-full flex flex-col gap-4 items-center">
        <h1 className="text-5xl">Welcome to Hotel!</h1>
        <p className="text-xl">Pick an action you would like to do</p>
      </div>
      <div className="w-full flex gap-8">
        <Card className="transition hover:shadow hover:border-r-foreground hover:border-b-foreground">
          <CardHeader>
            <CardTitle>Customers</CardTitle>
            <CardDescription>Edit customers</CardDescription>
          </CardHeader>
          <CardContent>
            <p>View, add, delete existing customers and add new ones</p>
          </CardContent>
          <CardFooter>
            <a href="/customers">
              <Button variant="outline">Select</Button>
            </a>
          </CardFooter>
        </Card>
        <Card className="transition hover:shadow hover:border-r-foreground hover:border-b-foreground">
          <CardHeader>
            <CardTitle>Rooms</CardTitle>
            <CardDescription>Edit rooms</CardDescription>
          </CardHeader>
          <CardContent>
            <p>View, add, delete existing rooms and add new ones</p>
          </CardContent>
          <CardFooter>
            <a href="/rooms">
              <Button variant="outline">Select</Button>
            </a>
          </CardFooter>
        </Card>
        <Card className="transition hover:shadow hover:border-r-foreground hover:border-b-foreground">
          <CardHeader>
            <CardTitle>Bookings</CardTitle>
            <CardDescription>Edit bookings</CardDescription>
          </CardHeader>
          <CardContent>
            <p>View, add, delete existing bookings and add new ones</p>
          </CardContent>
          <CardFooter>
            <a href="/bookings">
              <Button variant="outline">Select</Button>
            </a>
          </CardFooter>
        </Card>
      </div>
    </main>
  );
}
