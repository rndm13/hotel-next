"use client";

import { useState, Dispatch, SetStateAction, useEffect } from "react";
import { Input } from "../components/ui/input";
import { TableCell, TableRow } from "@/app/components/ui/table";
import { Button } from "../components/ui/button";
import { Textarea } from "../components/ui/textarea";
import { atom, useAtom, useSetAtom } from "jotai";

export type Customer = {
  id: number;
  first_name: string;
  last_name: string;
  comment: string;
  sale: number;
};
export const DefaultCustomer = {
  id: 0,
  first_name: "",
  last_name: "",
  comment: "",
  sale: 0,
};

export const customers = atom<Customer[]>([]);

export function CustomerInputRow({
  customerState,
  actions,
}: {
  customerState: [Customer, Dispatch<SetStateAction<Customer>>];
  actions: { title: string; onClick: (customer: Customer) => Customer }[];
}) {
  const [customer, setCustomer] = customerState;

  return (
    <TableRow>
      <TableCell className="align-top">
        {customer.id === 0 ? "" : customer.id}
      </TableCell>
      <TableCell className="align-top">
        <Input
          placeholder="Lorem"
          value={customer.first_name}
          onChange={(e) =>
            setCustomer((old) => {
              return { ...old, first_name: e.target.value };
            })
          }
        />
      </TableCell>
      <TableCell className="align-top">
        <Input
          placeholder="Ipsum"
          value={customer.last_name}
          onChange={(e) =>
            setCustomer((old) => {
              return { ...old, last_name: e.target.value };
            })
          }
        />
      </TableCell>
      <TableCell className="align-top">
        <Textarea
          className="h-full"
          placeholder="Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat."
          value={customer.comment}
          onChange={(e) =>
            setCustomer((old) => {
              return { ...old, comment: e.target.value };
            })
          }
        />
      </TableCell>
      <TableCell className="align-top"></TableCell>
      <TableCell className="flex gap-2">
        {actions.map((action, i) => {
          return (
            <Button
              key={i}
              variant={i === 0 ? "default" : "secondary"}
              onClick={() => {
                setCustomer(action.onClick(customer));
              }}
              className="grow"
            >
              {action.title}
            </Button>
          );
        })}
      </TableCell>
    </TableRow>
  );
}

export function AddCustomerRow() {
  const setCustomers = useSetAtom(customers);
  const customerState = useState<Customer>(DefaultCustomer);

  const onAdd = (customer: Customer) => {
    var inserted = 0;
    setCustomers((old) => {
      inserted = old.length;
      return old.concat([customer]);
    });
    const request = new Request("http://127.0.0.1:1323/customers", {
      method: "POST",
      body: JSON.stringify(customer),
      headers: {
        "Content-Type": "application/json",
      },
    });
    fetch(request).then(async (response) => {
      const json = await response.json();
      setCustomers((old) => old.map((o, i) => (i === inserted ? json : o)));
    });
    return DefaultCustomer;
  };
  const onClear = (_: Customer) => {
    return DefaultCustomer;
  };
  return (
    <CustomerInputRow
      customerState={customerState}
      actions={[
        { title: "Add", onClick: onAdd },
        { title: "Clear", onClick: onClear },
      ]}
    />
  );
}

export function CustomerRow({ customer }: { customer: Customer }) {
  const setCustomers = useSetAtom(customers);
  const [edit, setEdit] = useState<Customer>(DefaultCustomer);

  const onEdit = () => {
    setEdit(customer);
  };

  const onDelete = () => {
    setCustomers((old) => old.filter((o) => o.id !== customer.id));

    fetch("http://127.0.0.1:1323/customers/" + customer.id, {
      method: "DELETE",
    });
  };

  const onSave = (customer: Customer) => {
    setCustomers((old) =>
      old.map((o) => (o.id === customer.id ? customer : o)),
    );
    setEdit(DefaultCustomer);

    fetch("http://127.0.0.1:1323/customers/" + customer.id, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(customer),
    });

    return DefaultCustomer;
  };

  const onCancel = (_: Customer) => {
    setEdit(DefaultCustomer);

    return DefaultCustomer;
  };

  if (edit.id !== 0) {
    return (
      <CustomerInputRow
        customerState={[edit, setEdit]}
        actions={[
          { title: "Save", onClick: onSave },
          { title: "Cancel", onClick: onCancel },
        ]}
      />
    );
  }

  return (
    <TableRow>
      <TableCell className="font-medium">{customer.id}</TableCell>
      <TableCell>{customer.first_name}</TableCell>
      <TableCell>{customer.last_name}</TableCell>
      <TableCell>{customer.comment}</TableCell>
      <TableCell>{customer.sale === 0 ? "" : customer.sale + "%"}</TableCell>
      <TableCell className="flex gap-2">
        <Button className="grow rounded" onClick={onEdit}>
          Edit
        </Button>
        <Button
          className="grow rounded"
          variant="destructive"
          onClick={onDelete}
        >
          Delete
        </Button>
      </TableCell>
    </TableRow>
  );
}

export function CustomerList({ initial }: { initial: Customer[] }) {
  const [items, setItems] = useAtom(customers);

  useEffect(() => {
    setItems(initial);
  }, []);

  return items.map((customer) => <CustomerRow customer={customer} key={customer.id} />);
}
