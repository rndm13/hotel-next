"use server";

import {
  Table,
  TableBody,
  TableCaption,
  TableHead,
  TableHeader,
  TableRow,
} from "@/app/components/ui/table";
import { AddCustomerRow, CustomerList, Customer } from "./customers";

export default async function Customers() {
  const customers: Customer[] = await fetch("http://127.0.0.1:1323/customers", {
    // next: { tags: ["customers"] },
    cache: "no-store",
  }).then((response) => response.json());

  return (
    <div className="m-24 rounded p-2 border shadow">
      <Table>
        <TableCaption>A list of your customers.</TableCaption>
        <TableHeader>
          <TableRow>
            <TableHead className="w-10">Id</TableHead>
            <TableHead>First name</TableHead>
            <TableHead>Last name</TableHead>
            <TableHead>Comment</TableHead>
            <TableHead>Sale</TableHead>
            <TableHead>Actions</TableHead>
          </TableRow>
        </TableHeader>
        <TableBody>
          <CustomerList initial={customers} />
          <AddCustomerRow />
        </TableBody>
      </Table>
    </div>
  );
}
